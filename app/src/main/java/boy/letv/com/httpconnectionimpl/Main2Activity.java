package boy.letv.com.httpconnectionimpl;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MotionEvent;

public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inBitmap = null;//设置可以复用的bitmap资源
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        dispatchTouchEvent(event);
        return super.onTouchEvent(event);
    }
}
