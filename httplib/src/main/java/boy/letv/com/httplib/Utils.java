package boy.letv.com.httplib;

import java.util.Map;

/**
 * Created by zhangxiaang on 17/7/13.
 */

public class Utils {
    public static String formatParams(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        if (params == null || params.size() == 0) {
            return sb.toString();
        }
        for (String key : params.keySet()) {
            sb.append(key).append("=").append(params.get(key)).append("&");
        }
        return sb.toString().substring(0, sb.length() - 1);
    }
}