package boy.letv.com.httplib;

/**
 * Created by zhangxiaang on 17/7/7.
 */

public interface AsyncCallback {
    //todo put the result into response
    void onSuccess(String response);

    void onFailed(String errorMsg);
}
