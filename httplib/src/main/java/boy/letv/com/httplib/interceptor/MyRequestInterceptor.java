package boy.letv.com.httplib.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import boy.letv.com.httplib.HttpClient;
import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;
import boy.letv.com.httplib.call.SyncCall;

//所以看到就是  所有的Request操作都在interceptor里面完成的
public class MyRequestInterceptor implements Interceptor {
    public final HttpClient client;

    public MyRequestInterceptor(HttpClient client) {
        this.client = client;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        //todo 获取所有的interceptors列表 目前只暂定是一个requestInterceptor
        StringBuilder buffer = new StringBuilder();
        BufferedReader read = null;
        HttpURLConnection connection = client.getConnection(request);
        int statusCode = -1;
        try {
            statusCode = connection.getResponseCode();
            switch (statusCode) {
                case 300:
                case 301:
                case 302:
                case 303:
                case 307:
                case 308://处理重定向的问题
                    return client.sendCall(new SyncCall(client, new Request.Builder()
                            .setMethod(request.getMethod())
                            .setUrl(client.handleRequestRedirect(connection))
                            .setBody(request.getBody())
                            .build()));
                default:
                    InputStream stream = connection.getInputStream();
                    read = new BufferedReader(new InputStreamReader(stream));
                    String line;
                    while ((line = read.readLine()) != null) {
                        buffer.append(line).append('\n');
                    }
                    break;
            }
        } catch (IOException | NullPointerException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            if (read != null) {
                try {
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new Response.Builder()
                .setRequest(request)
                .setBody(buffer.toString())
                .setCode(statusCode)
                .build();
    }

    public static class MyRequestChain implements MyRequestInterceptor.Chain {
        private Request request;
        private MyRequestInterceptor interceptor;

        public MyRequestChain(Request request, MyRequestInterceptor interceptor) {
            this.request = request;
            this.interceptor = interceptor;
        }

        @Override
        public Request request() {
            return this.request;
        }

        @Override
        public Response proceed(Request request) throws IOException {
            return interceptor.intercept(this);
        }
    }
}
