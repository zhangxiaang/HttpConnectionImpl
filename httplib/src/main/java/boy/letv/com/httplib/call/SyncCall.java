package boy.letv.com.httplib.call;

import java.io.IOException;

import boy.letv.com.httplib.HttpClient;
import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;

//同步的call不涉及进队列 所以execute()保持空就可以, 反正钓不到里面的内容
public class SyncCall implements Call {
    private Request request;
    private HttpClient client;
    private boolean executed;

    public SyncCall(HttpClient client, Request request) {
        this.client = client;
        this.request = request;
    }

    @Override
    public Request request() {
        return this.request;
    }

    @Override
    public Response execute() throws IOException {
        return null;
    }

    @Override
    public void enqueue(Request request) {

    }

    @Override
    public Call clone() {
        return null;
    }
}
