package boy.letv.com.httplib.interceptor;

import java.io.IOException;

import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;

/**
 * Created by zhangxiaang on 17/7/13.
 */
public interface Interceptor {
    Response intercept(Chain chain) throws IOException;

    //这个chain起的是什么作用呢?
    interface Chain {
        Request request();

        Response proceed(Request request) throws IOException;
    }
}
