package boy.letv.com.httplib;

/**
 * 作为receive / format network result的一个容器
 * 如何把network返回来的response str装入此容器中
 */

public class Response {
    final Request request;
    final int code;//status code
    final String message;
    final Headers headers;
    final Response networkResponse;//can use cache reponse
    final long sendRequestAtMills;
    final long receiveRequestAtMills;
    final String body;

    Response(Builder builder) {
        this.request = builder.request;
        this.code = builder.code;
        this.headers = builder.headers;
        this.message = builder.message;
        this.networkResponse = builder.networkResponse;
        this.sendRequestAtMills = builder.sendRequestAtMills;
        this.receiveRequestAtMills = builder.receiveRequestAtMills;
        this.body = builder.body;
    }

    public String header(String name) {
        return header(name, null);
    }

    public String header(String name, String defaultValue) {
        String result = headers.get(name);
        if (result == null) {
            return defaultValue;
        }
        return result;
    }

    public String getBody() {
        return this.body;
    }

    public boolean isSuccessfull() {
        return code >= 200 && code < 300;
    }

    public static class Builder {
        Request request;
        int code = -1;//status code
        String message;
        Headers headers;
        Response networkResponse;//can use cache reponse
        long sendRequestAtMills;
        long receiveRequestAtMills;
        String body;

        public Response build() {
            return new Response(this);
        }

        public Builder setBody(String body) {
            this.body = body;
            return this;
        }

        public Builder setCode(int code) {
            this.code = code;
            return this;
        }

        public Builder setRequest(Request request) {
            this.request = request;
            return this;
        }
    }
}
