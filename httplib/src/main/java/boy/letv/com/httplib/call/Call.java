package boy.letv.com.httplib.call;

import java.io.IOException;

import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;

//类似一个Request的容器
//它的几个方法实际上目前都没有调用到  那么这个容器的存在意义是什么呢?
public interface Call extends Cloneable {
    Request request();

    Response execute() throws IOException;

    void enqueue(Request request);

    Call clone();

//    interface Factory {
//        Call newCall(Request request);
//    }
}
