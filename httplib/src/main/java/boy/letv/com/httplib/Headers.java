package boy.letv.com.httplib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * headers for request and response
 * Builder pattern
 */
public class Headers {
    private final String[] namesAndValues;//这个一旦初始化就定下来了...

    Headers(Builder builder) {
        this.namesAndValues = builder.namesAndValues.toArray(new String[builder.namesAndValues.size()]);
    }

    public Headers(String[] pairs) {
        this.namesAndValues = pairs;
    }

    public Builder newBuilder() {
        Builder result = new Builder();
        Collections.addAll(result.namesAndValues, namesAndValues);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            builder.append(name(i)).append(": ").append(value(i)).append('\n');
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Headers)) {
            return false;
        }
        return Arrays.equals(((Headers) obj).namesAndValues, namesAndValues);
    }

    public int size() {
        return namesAndValues.length / 2;
    }

    private String name(int index) {
        if (index > size()) {
            throw new ArrayIndexOutOfBoundsException("index is out of the array size!");
        }
        return namesAndValues[index * 2];
    }

    private Set<String> names() {
        Set<String> result = new HashSet<>();
        for (int i = 0; i < size(); i++) {
            result.add(name(i));
        }
        return Collections.unmodifiableSet(result);
    }

    private List<String> values() {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < size(); i++) {
            result.add(value(i));
        }
        return Collections.unmodifiableList(result);
    }

    public String get(String name) {
        return get(namesAndValues, name);
    }

    private String get(String[] namesAndValues, String name) {
        for (int i = namesAndValues.length - 2; i >= 0; i -= 2) {
            if (name.equalsIgnoreCase(namesAndValues[i])) {
                return namesAndValues[i + 1];
            }
        }
        return null;
    }

    private String value(int index) {
        if (index > size()) {
            throw new ArrayIndexOutOfBoundsException("index is out of the array size!");
        }
        return namesAndValues[index * 2 + 1];
    }

    //pair CRUD
    public static final class Builder {
        final ArrayList<String> namesAndValues = new ArrayList<>(32);

        public Builder add(String line) {
            if (line == null) {
                line = "";
            }
            int splitIndex = line.indexOf(':');
            if (splitIndex == -1) {
                throw new IllegalArgumentException("the line of header:" + line + " is Illegal!");
            }
            return add(line.substring(0, splitIndex).trim(), line.substring(splitIndex + 1));
        }

        public Builder add(String name, String value) {
            checkNameAndValue(name, value);
            addIntoArray(name, value);
            return this;
        }

        public Builder remove(String name) {
            if (name == null || name.isEmpty()) {
                return this;
            }
            for (int i = 0; i < namesAndValues.size(); i += 2) {
                if (name.equalsIgnoreCase(namesAndValues.get(i))) { // remove all contains the name
                    namesAndValues.remove(i);//name
                    namesAndValues.remove(i);//value
                    i -= 2;
                }
            }
            return this;
        }

        public Builder set(String name, String value) {
            checkNameAndValue(name, value);
            remove(name);
            return add(name, value);
        }

        public String get(String name) {
            checkName(name);
            for (int i = 0; i < namesAndValues.size(); i += 2) {
                if (name.equalsIgnoreCase(namesAndValues.get(i))) {
                    return namesAndValues.get(i + 1);
                }
            }
            return null;
        }

        private Builder addIntoArray(String name, String value) {
            this.namesAndValues.add(name.trim());
            this.namesAndValues.add(value.trim());
            return this;
        }

        private void checkNameAndValue(String name, String value) {
            checkName(name);
            checkValue(name, value);
        }

        private void checkName(String name) {
            if (name == null || name.isEmpty())
                throw new IllegalArgumentException("the name should not be null or empty!");
            //check the name key valid
            for (int i = 0, length = name.length(); i < length; i++) {
                char c = name.charAt(i);
                if (c <= '\u0020' || c >= '\u007f') {
                    throw new IllegalArgumentException(String.format(Locale.ENGLISH,
                            "Unexpected char %#04x at %d in header name: %s", (int) c, i, name));
                }
            }
        }

        private void checkValue(String name, String value) {
            if (value == null)
                throw new NullPointerException("the value should not be null!");
            //check the value is valid
            for (int i = 0, length = value.length(); i < length; i++) {
                char c = value.charAt(i);
                if ((c <= '\u001f' && c != '\t') || c >= '\u007f') {
                    throw new IllegalArgumentException(String.format(Locale.ENGLISH,
                            "Unexpected char %#04x at %d in %s value: %s", (int) c, i, name, value));
                }
            }
        }

        public Headers build() {
            return new Headers(this);
        }
    }
}
