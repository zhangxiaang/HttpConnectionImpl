package boy.letv.com.httplib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import boy.letv.com.httplib.call.SyncCall;
import boy.letv.com.httplib.interceptor.Interceptor;
import boy.letv.com.httplib.interceptor.MyRequestInterceptor;

public class HttpClient {
    private static volatile HttpClient mInstance;
    private boolean isSupportRedirect = true;
    private List<Interceptor> interceptors = new ArrayList<>();
    private Dispatcher dispatcher;//负责request的派发工作

    public static synchronized HttpClient getInstance() {
        if (mInstance == null) {
            mInstance = new HttpClient();
        }
        return mInstance;
    }

    public void setSupportRedirect(boolean support) {
        this.isSupportRedirect = support;
    }

    public boolean supportRedirect() {
        return this.isSupportRedirect;
    }

    private HttpClient() {
        dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(32);
    }

    public Response sendCall(SyncCall syncCall) {
        return enqueueSyncCall(syncCall);//应该把call丢进一个queue中去吧
    }

    public void sendRequestAsync(Request request, AsyncCallback callback) {
        executeRequestAsync(request, callback);
    }

    public String handleRequestRedirect(HttpURLConnection connection) {
        if (!supportRedirect()) {
            return null;
        }
        String redirectUrl = "";
        Map<String, List<String>> headers = connection.getHeaderFields();
        Set<String> res = headers.keySet();
        for (String str : res) {
            if ("Location".equals(str)) {
                redirectUrl = headers.get(str).get(0);
            }
        }
        return redirectUrl;
    }

    private Response getResponseWithInterceptorChain(Request request) {
        List<Interceptor> interceptors = getInterceptors();
        //假设只有一个interceptor
        MyRequestInterceptor interceptor = (MyRequestInterceptor) interceptors.get(0);
        MyRequestInterceptor.MyRequestChain chain = new MyRequestInterceptor.MyRequestChain(request, interceptor);
        try {
            return chain.proceed(request);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    private Response enqueueSyncCall(SyncCall call) {
        getDispatcher().executeSync(call);
        return getResponseWithInterceptorChain(call.request());
    }

    //Async request
    private void executeRequestAsync(final Request request, final AsyncCallback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection connection = getConnection(request);
                if (connection == null) {
                    callback.onFailed("open Connection failed");
                    return;
                }
                int statusCode = -1;
                try {
                    statusCode = connection.getResponseCode();
                    switch (statusCode) {
                        case 300:
                        case 301:
                        case 302:
                        case 303:
                        case 307:
                        case 308:
                            Request redirectRequest = new Request.Builder()
                                    .setMethod(request.getMethod())
                                    .setUrl(handleRequestRedirect(connection))
                                    .setBody(request.getBody())
                                    .build();
                            executeRequestAsync(redirectRequest, callback);
                            break;
                        default:
                            StringBuilder buffer = new StringBuilder();
                            InputStream is = connection.getInputStream();
                            BufferedReader read = new BufferedReader(new InputStreamReader(is));

                            String line;
                            while ((line = read.readLine()) != null) {
                                buffer.append(line).append('\n');
                            }
                            callback.onSuccess(buffer.toString());
                    }
                } catch (IOException e) {
                    callback.onFailed(e.getMessage());
                }
            }
        }).start();
    }


    /**
     * 根据传入的request的参数, 写入到request headers里面去
     */
    public HttpURLConnection getConnection(Request request) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(request.getUrl());
            connection = (HttpURLConnection) url.openConnection();
            HttpMethod method = request.getMethod();

            if (method == HttpMethod.POST) {
                Map<String, String> params = request.getBody();
                String paramsStr = Utils.formatParams(params);
                int postParamsLength = paramsStr.length();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setInstanceFollowRedirects(false);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("Content-Length", Integer.toString(postParamsLength));
                connection.setUseCaches(false);
                DataOutputStream stream = new DataOutputStream(connection.getOutputStream());
                stream.write(paramsStr.getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    public void addInterceptor(Interceptor interceptor) {
        if (!interceptors.contains(interceptor)) {
            interceptors.add(interceptor);
        }
    }

    public List<Interceptor> getInterceptors() {
        return this.interceptors;
    }
}
