package boy.letv.com.httplib;

import java.util.Map;

/**
 * Builder pattern
 */
public class Request {
    private String url;
    private HttpMethod method;
    private Headers header;
    private Map<String, String> body;
    private Object tag;

    public String getUrl() {
        return url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public Headers getHeader() {
        return header;
    }

    public Map<String, String> getBody() {
        return body;
    }

    public Object getTag() {
        return tag;
    }

    public boolean isHttps() {
        return this.url.trim().startsWith("https");
    }

    public Request(Builder builder) {
        this.url = builder.url;
        this.method = builder.method;
        this.body = builder.body;
//        this.header = builder.headerBuilder.build();
//        this.tag = builder.tag != null ? builder.tag : this;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    public static class Builder {
        String url;
        HttpMethod method;
        Headers.Builder headerBuilder;
        Map<String, String> body;
        Object tag;

        public Builder() {
            this.method = HttpMethod.GET;
        }

        public Builder(Request request) {
            this.url = request.url;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setBody(Map<String, String> params) {
            this.body = params;
            return this;
        }

        public Builder setMethod(HttpMethod method) {
            this.method = method;
            return this;
        }

        public Builder addHeader(String name, String value) {
            this.headerBuilder.add(name, value);
            return this;
        }

        public Builder removeHeader(String name) {
            this.headerBuilder.remove(name);
            return this;
        }

        public Request build() {
            return new Request(this);
        }
    }
}
