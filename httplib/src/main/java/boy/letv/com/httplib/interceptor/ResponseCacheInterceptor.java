package boy.letv.com.httplib.interceptor;

import android.util.LruCache;

import java.io.IOException;

import boy.letv.com.httplib.HttpClient;
import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;

public class ResponseCacheInterceptor implements Interceptor {
    private static LruCache<String, Response> responseLruCache = new LruCache<>(32);
    private HttpClient client;

    public ResponseCacheInterceptor(HttpClient client) {
        this.client = client;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response cache = responseLruCache == null ? null : responseLruCache.get(request.getUrl());

        if (checkoutNetwork()) {

        }

        //拦截的流程为:
        // 无网络 构造一个response返回
        // 有网络 无缓存  发送请求获取response 存入到cache
        // 有网络 有缓存  取出缓存 检查缓存是否实效  若失效走第二部
        // 若没失效  把缓存作为结果返回
        return null;
    }

    //模拟是否有网络
    private boolean checkoutNetwork() {
        return true;
    }
}
