package boy.letv.com.httplib;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import boy.letv.com.httplib.call.AsyncCall;
import boy.letv.com.httplib.call.SyncCall;

public final class Dispatcher {
    private int maxRequests = 32;
    private Deque<SyncCall> runningSyncCalls = new ArrayDeque<>();

    private Deque<AsyncCall> runningAsyncCalls = new ArrayDeque<>();
    private Deque<AsyncCall> readyAsyncCalls = new ArrayDeque<>();
    private ExecutorService service;

    Dispatcher() {
    }

    Dispatcher(ExecutorService service) {
        this.service = service;
    }

    void setMaxRequests(int maxRequests) {
        if (maxRequests < 1) {
            throw new IllegalArgumentException("the max num should more than 1");
        }
        this.maxRequests = maxRequests;
    }

    private synchronized ExecutorService executorService() {
        if (this.service == null) {
            // 使用默认的线程工厂类来生产默认的thread
            this.service = new ThreadPoolExecutor(3, 5,
                    60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
        }
        return this.service;
    }

    //其实这个入队列的操作就是让一个子线程来立即执行这个runnable 和Looper+MQ的死循环是不相同的
    public synchronized void enqueue(AsyncCall call) {
        //先判断下运行队列能不能执行新加进来的call  否则丢到ready队列中去
        if (runningAsyncCalls.size() < maxRequests) {
            runningAsyncCalls.add(call);
            executorService().execute(call);//线程池去调度执行 call
        } else {
            readyAsyncCalls.add(call);
        }
    }

    //可见dispatcher对于同步请求并没有什么额外的操作, 只是简单的纪录一下状态
    public synchronized void executeSync(SyncCall call) {
        runningSyncCalls.add(call);//添加进队列里去了 谁来执行?
    }
}
