package boy.letv.com.httplib.call;

import java.io.IOException;
import java.util.List;

import boy.letv.com.httplib.AsyncCallback;
import boy.letv.com.httplib.HttpClient;
import boy.letv.com.httplib.interceptor.Interceptor;
import boy.letv.com.httplib.interceptor.MyRequestInterceptor;
import boy.letv.com.httplib.Request;
import boy.letv.com.httplib.Response;

/**
 * Created by zhangxiaang on 17/7/13.
 * 可见异步的Request其实是一个可执行的task
 */

public class AsyncCall implements Runnable {
    private HttpClient client;
    private Request request;
    private AsyncCallback callback;

    public AsyncCall(HttpClient client, Request request, AsyncCallback callback) {
        this.client = client;
        this.request = request;
        this.callback = callback;
    }

    @Override
    public Call clone() {
        return null;
    }

    @Override
    public void run() {
        executes();
    }

    private void executes() {
        Response response = getResponseByChain();
        callback.onSuccess(response.getBody());
    }

    private Response getResponseByChain() {
        List<Interceptor> interceptors = client.getInterceptors();//如何在这里拿到client的interceptors呢?
        MyRequestInterceptor interceptor = (MyRequestInterceptor) interceptors.get(0);
        MyRequestInterceptor.MyRequestChain chain = new MyRequestInterceptor.MyRequestChain(request, interceptor);
        try {
            return chain.proceed(request);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }
}
