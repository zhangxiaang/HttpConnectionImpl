package boy.letv.com.httplib;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import boy.letv.com.httplib.call.AsyncCall;
import boy.letv.com.httplib.call.SyncCall;
import boy.letv.com.httplib.interceptor.Interceptor;
import boy.letv.com.httplib.interceptor.MyRequestInterceptor;

public class ExampleUnitTest {

    @Test
    public void testRequestRedirect() throws Exception {
        HttpClient client = HttpClient.getInstance();
        //尝试添加自定义的 request 拦截器  开心!!!
        Interceptor interceptor = new MyRequestInterceptor(client);
        client.addInterceptor(interceptor);

        Request request = new Request.Builder().setUrl("http://www.publicobject.com/helloworld.txt").build();
        Response result = client.sendCall(new SyncCall(client, request));

        System.out.println(result.body);
    }

    @Test
    public void testSendRequest() throws Exception {
        HttpClient client = HttpClient.getInstance();
        //尝试添加自定义的 request 拦截器  开心!!!
        Interceptor interceptor = new MyRequestInterceptor(client);
        client.addInterceptor(interceptor);

        Request request = new Request.Builder().setUrl("http://gank.io/api/data/Android/10/1").build();
        Response result = client.sendCall(new SyncCall(client, request));

        System.out.println(result.body);
    }

    @Test
    public void sendHttpsRequest() throws Exception {
        HttpClient client = HttpClient.getInstance();
        Request request = new Request.Builder().setUrl("https://www.zhihu.com/").build();
        Response result = client.sendCall(new SyncCall(client, request));
        System.out.println(result.body);
    }

    @Test
    public void testSendPostRequest() throws Exception {
        HttpClient client = HttpClient.getInstance();
        Map<String, String> param = new HashMap<>();
        param.put("type", "text");
        param.put("content", "hello world");

        Request request = new Request.Builder()
                .setUrl("http://rest.sinaapp.com/api/post")
                .setBody(param)
                .setMethod(HttpMethod.POST)
                .build();
        Response result = client.sendCall(new SyncCall(client, request));
        System.out.println(result.body);
    }

    @Test
    public void testSendRequestAsync() throws Exception {
        HttpClient client = HttpClient.getInstance();
        Interceptor interceptor = new MyRequestInterceptor(client);
        client.addInterceptor(interceptor);
        Request request = new Request.Builder()
                .setUrl("http://gank.io/api/data/Android/10/1")
                .setMethod(HttpMethod.GET)
                .build();
        AsyncCall asyncCall = new AsyncCall(client, request, new AsyncCallback() {
            @Override
            public void onSuccess(String response) {
                System.out.println(response);
            }

            @Override
            public void onFailed(String errorMsg) {
                System.err.println(errorMsg);
            }
        });
        client.getDispatcher().enqueue(asyncCall);//添加到队列里面去  然后让子线程自己来run 任务task

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                System.out.println((++count) / 10.0f + "S is over...");
            }
        }, 1000, 100);

        Thread.currentThread().sleep(5000L);
    }

    @Test
    public void testPost() throws Exception {
        HttpClient client = HttpClient.getInstance();
        Map<String, String> param = new HashMap<>();
        param.put("type", "text");
        param.put("content", "hello world");

        Request request = new Request.Builder()
                .setUrl("http://rest.sinaapp.com/api/post")
                .setBody(param)
                .setMethod(HttpMethod.POST)
                .build();

        client.sendRequestAsync(request, new AsyncCallback() {
            @Override
            public void onSuccess(String response) {
                System.out.println(response);
            }

            @Override
            public void onFailed(String errorMsg) {
                System.err.println(errorMsg);
            }
        });

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                System.out.println((++count) / 10.0f + "S is over...");
            }
        }, 1000, 100);

        Thread.currentThread().sleep(5000L);
    }
}