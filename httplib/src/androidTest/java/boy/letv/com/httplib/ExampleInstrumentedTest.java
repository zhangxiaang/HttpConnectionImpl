package boy.letv.com.httplib;

import android.content.Context;
import android.os.AsyncTask;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        AsyncTask task = new MyAsyncTask();
        Log.e("TAG", "before run task : in thread:" + Thread.currentThread().getName());
        task.execute("http://gank.io/api/data/Android/10/1");
    }

    class MyAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.e("TAG", "when run task : in thread:" + Thread.currentThread().getName());
            StringBuffer buffer = new StringBuffer();
            try {
                URL url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                InputStream is = connection.getInputStream();
                BufferedReader read = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = read.readLine()) != null) {
                    buffer.append(line);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return buffer.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("TAG", "result:" + s);
        }
    }
}
